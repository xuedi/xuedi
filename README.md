[![Platform](https://img.shields.io/static/v1?label=Platform&message=GNU%20Linux%20x86%20since%201997&color=green)](https://archlinux.org/)
[![Version](https://img.shields.io/static/v1?label=Version&message=3.1&color=green)](https://github.com/xuedi/xuedi/blob/main/VERSION.md)
[![Issues](https://img.shields.io/github/issues/xuedi/xuedi)](https://github.com/xuedi/xuedi/issues)
[![Health](https://img.shields.io/static/v1?label=Packagesize&message=average%20need%20sport&color=red)](https://github.com/xuedi/xuedi/blob/main/HEALTH.md)
[![Rating](https://img.shields.io/static/v1?label=Rating&message=★★★★☆&color=green)](https://github.com/xuedi/xuedi/blob/main/RATING.md)
[![Funding](https://img.shields.io/static/v1?label=XCH&message=xch1xg4tgnfgvk6ae8tw3yp7kd8t9rwsnxty6jfw7nh6gkffl2zd5jcse8fj4j&color=green)](https://www.chia.net/greenpaper/)





