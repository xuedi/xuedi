
### version history
 - 0.1.00.1976 beta // Parents meet each other
 - 0.5.00.1981 alpha // Project commitment while drunk listening at a party to Gottlieb Wendehals: Polonäse
 - 1.0.00.1982 release // Right on production schedule
 - 1.1.12.1994 bugfix // fixed childhood issue with post puberty bugfix
 - 1.2.15.1997 release // production went live in first internet bubble 
 - 1.3.19.2001 refactoring // refactored out post bubble adjustments
 - 1.4.23.2005 release // added chinese locale feature
 - 1.5.30.2012 release // scraped chinese feature and changed core to german local
 - 2.0.32.2014 release // integrated local social features
 - 2.1.34.2016 release // added shit tons of social features and tuned up engine to over 9000
 - 3.0.36.2018 bugfix // rebalanced social features by pull request @friends
 - 3.1.40.2022 release // integrated pachset 40 with readjustments

### roadmap
 - 3.2 - planed new patch with increasing sport level by 5x
